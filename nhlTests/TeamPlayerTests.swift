//
//  TeamServiceTests.swift
//  nhlTests
//
//  Created by Daniel Eduardo Verissimo De Castro S DAbadia on 29/09/19.
//  Copyright © 2019 Daniel Eduardo Verissimo De Castro S DAbadia. All rights reserved.
//

import XCTest
@testable import nhl
@testable import Alamofire

class TeamPlayerTests: XCTestCase {

    var teamStore: TeamStore!
    var playerStore: PlayerStore!
    
    var travisZajac: Player!
    var andyGreene: Player!
    var nikitaGusev: Player!
        
    override func setUp() {
        
        travisZajac = Player(dict: ["jerseyNumber": "19", "person": ["id": 8471233, "fullName": "Travis Zajac", "link": "/api/v1/people/8471233", "nationality": "CAN"], "position": ["code": "G", "name": "Goalie", "type": "Goalie", "abbreviation": "G"]]);
        
        andyGreene = Player(dict: ["jerseyNumber": "35", "person": ["id": 8472382, "fullName": "Andy Greene", "link": "/api/v1/people/8472382", "nationality": "CAN"], "position": ["code": "D", "name": "Defenseman", "type": "Defenseman", "abbreviation": "D"]]);
        
        nikitaGusev = Player(dict: ["jerseyNumber": "97", "person": ["id": 8477038, "fullName": "Nikita Gusev", "link": "/api/v1/people/8477355", "nationality": "USA"], "position": ["code": "D", "name": "Defenseman", "type": "Defenseman", "abbreviation": "D"]]);
        
        teamStore = TeamStore()
        playerStore = PlayerStore()
        
    }

    override func tearDown() {
        playerStore = nil
    }
    
    // MARK: - Sort Test
    
    func testePlayersSortedOriginalOrder() {
            
        playerStore.players.append(travisZajac)
        playerStore.players.append(nikitaGusev)
        playerStore.players.append(andyGreene)
        
        XCTAssert(playerStore.players[2].person.id == 8472382)
        
    }
    
    func testePlayersSortedByJerseyNumber() {
            
        playerStore.players.append(travisZajac)
        playerStore.players.append(nikitaGusev)
        playerStore.players.append(andyGreene)
        
        playerStore.sortCriteria = "jerseyNumber"
        playerStore.sortPlayers()
        
        XCTAssert(playerStore.players[0].person.id == 8471233)
        
    }
    
    func testePlayersSortedByName() {
        
        playerStore.players.append(travisZajac)
        playerStore.players.append(nikitaGusev)
        playerStore.players.append(andyGreene)
        
        playerStore.sortCriteria = "name"
        playerStore.sortPlayers()
        
        XCTAssert(playerStore.players[0].person.id == 8472382)
        
    }
    
    // MARK: - Filter Test
    
    func testePlayersFilteredByGoaliePosition() {
        
        playerStore.playersFilter.append(travisZajac)
        playerStore.playersFilter.append(nikitaGusev)
        playerStore.playersFilter.append(andyGreene)
        
        playerStore.filterBy(code: "G")
        
        XCTAssert(playerStore.players.count == 1)
    }
    
    func testePlayersFilteredByDefensemanPosition() {
        
        playerStore.playersFilter.append(travisZajac)
        playerStore.playersFilter.append(nikitaGusev)
        playerStore.playersFilter.append(andyGreene)
        
        playerStore.filterBy(code: "D")
        
        XCTAssert(playerStore.players.count == 2)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
