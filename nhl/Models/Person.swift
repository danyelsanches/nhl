import SwiftUI
import CoreLocation

class Person: Identifiable {
    
    let id: Int
    let fullName: String
    let link: String
    let nationality: String?
    
    init(dict: [String: Any]) {
        self.id = dict["id"] as! Int;
        self.fullName = dict["fullName"] as! String;
        self.link = dict["link"] as! String;
        self.nationality = dict["nationality"] as? String;
    }
    
    var nationalityFlag: Image {
        Image(String(nationality?.prefix(2) ?? ""))
    }
}
