import SwiftUI
import CoreLocation

class Team: Decodable, Identifiable{
    let id: Int
    let name: String
    let shortName: String
    let abbreviation: String
    
    init(dict: [String: Any]) {
        self.id = dict["id"] as! Int;
        self.name = dict["name"] as! String;
        self.shortName = dict["shortName"] as! String;
        self.abbreviation = dict["abbreviation"] as! String;
    }
    
    var image: Image {
        Image(shortName)
    }
}
