import SwiftUI
import CoreLocation

class Position{
    
    let code: String
    let name: String
    let type: String
    let abbreviation: String
    
    init(dict: [String: Any]) {
        self.code = dict["code"] as! String;
        self.name = dict["name"] as! String;
        self.type = dict["type"] as! String;
        self.abbreviation = dict["abbreviation"] as! String;
    }
}
