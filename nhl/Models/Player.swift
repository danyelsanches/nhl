import SwiftUI
import CoreLocation

class Player: Identifiable{
    
    let jerseyNumber: String
    let person: Person
    let position: Position
    
    init(dict: [String: Any]) {
        self.person = Person(dict: dict["person"] as! [String : Any]);
        self.position = Position(dict: dict["position"] as! [String : Any]);
        
        if ( dict.keys.contains("jerseyNumber") ) {
            self.jerseyNumber = dict["jerseyNumber"] as! String;
        } else {
            self.jerseyNumber = "0"
        }
    }
    
    var id: Int {
        Int(self.jerseyNumber) ?? 0
    }
}
