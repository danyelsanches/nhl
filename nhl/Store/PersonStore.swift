import Foundation
import Combine

public class PersonStore: ObservableObject {
    
    @Published var person: Person!

    // MARK: - Fetch
    
    func fetchPerson(personId: Int, onFinish:@escaping () -> ()) {
        
        PersonService.getPerson(personId: personId){ result in
            switch result {
            case .success(let person):
                self.person = (person as! Person)
                onFinish()
            case .failure(let error):
                print(error.localizedDescription)
                onFinish()
            }
        }
    }
}
