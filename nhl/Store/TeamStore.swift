import Foundation
import Combine

public class TeamStore: ObservableObject {
    
    @Published var teams: [Team] = []

    func fetchTeams(onFinish:@escaping (Bool) -> ()) {
        
        TeamService.getTeams{ result in
            switch result {
            case .success(let teams):
                self.teams = teams as! [Team];
                onFinish(true)
                break;
            case .failure(let error):
                self.teams = []
                print(error.localizedDescription)
                onFinish(false)
            }
        }
        
    }
    
    func addTeam(team: Team){
        self.teams.append(team)
    }
    
    func getTeams() -> [Team]{
        return self.teams
    }
    
}
