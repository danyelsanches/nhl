import Foundation
import Combine
import SwiftUI

public class PlayerStore: ObservableObject {
        
    @Published var players: [Player] = []
    @Published var playersFilter: [Player] = []
    @Published var sortCriteria: String = ""
    @Published var positionList: [Position] = []
    
    @Published var showPopoverFilter: Bool = false
    @Published var showPopoverOrder: Bool = false
    
    // MARK: - Fetch
    
    func fetchPlayers(teamId: Int, onFinish:@escaping () -> ()) {
        
        PlayerService.getPlayers(teamId: teamId){ result in
            switch result {
                case .success(let players):
                    
                    self.players = players as! [Player]
                    self.playersFilter = self.players
                    
                    self.players.forEach{r in
                        
                        let contain = self.positionList.contains(where: {$0.code == r.position.code});
                        if ( !contain ) {
                            self.positionList.append(r.position)
                        }
                    }
                    
                    onFinish()

                    break;

                case .failure(let error):
                    print(error.localizedDescription)
                    self.players = []
                    onFinish()
            }
        }
    }
    
    // MARK: - Sort
    
    /// Sort `players` by `name` or `jerseyNumber` sortCriteria attribute.
    func sortPlayers() -> Void{
       
       if ( self.sortCriteria == "name" ) {
           self.players.sort{$0.person.fullName < $1.person.fullName}
       } else if ( self.sortCriteria == "jerseyNumber" ) {
           self.players.sort{Int($0.jerseyNumber) ?? 0 < Int($1.jerseyNumber) ?? 0}
       }
       
       self.showPopoverOrder = false
       
    }
    
    // Filter player list by positon `code`.
    func filterBy(code: String) -> Void{

        self.players = self.playersFilter.filter{$0.position.code == code}

        // Keep player list sorted
        if ( self.sortCriteria != "" ) {
            self.sortPlayers()
        }

        self.showPopoverFilter = false
    }

    func resetFilter() -> Void{
        self.players = self.playersFilter
        
        // Keep player list sorted
        if ( self.sortCriteria != "" ) {
            self.sortPlayers()
        }
    
       self.showPopoverFilter = false
    }
    
    func playerMap() -> [Player]{
        return self.players.map { player -> Player in
            return player
        }
    }
}
