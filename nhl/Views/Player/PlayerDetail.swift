import SwiftUI

struct PlayerDetailView: View {

    @EnvironmentObject var personStore: PersonStore

    var onDismiss: () -> ()
    
    var body: some View {
        
        NavigationView {
            VStack {
                
                if ( self.personStore.person != nil ) {
                    Text(self.personStore.person.fullName).font(.largeTitle)
                    
                    Image(String(self.personStore.person.nationality!.prefix(2))).resizable().frame(width: 100, height: 70)
                    
                    Text(self.personStore.person.nationality!).font(.largeTitle)
                }
            
            }
            .navigationBarTitle(Text(""), displayMode: .automatic)
            .navigationBarItems(trailing: Button(action: {
                self.onDismiss()
            }, label: { Image("close").resizable().frame(width: 35, height: 35)}))
            
        }
    }
    
}

struct PlayerDetailView_Previews: PreviewProvider {

    static var previews: some View {

        PlayerDetailView(onDismiss: {})

    }
}
