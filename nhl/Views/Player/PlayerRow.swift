import SwiftUI

struct PlayerRow: View {
    var player: Player

    var body: some View {
        HStack {
                
            VStack(alignment: .leading){
                Text("# \(player.jerseyNumber)")
                    .font(.largeTitle)
            }.frame(width: 80, alignment: .leading)
            
            VStack(alignment: .leading){
                Text(player.person.fullName)
                Text(player.position.name)
                    .font(.subheadline)
                    .foregroundColor(.gray)
            }.padding(.horizontal, 10)
            
        }
    }
}

struct PlayerRow_Previews: PreviewProvider {
    static var previews: some View {

        PlayerRow(player: Player(dict: ["jerseyNumber": "1", "person": ["id": 1, "fullName": "Daniel Eduardo", "link": "/api/v1/people/8481601"]]))
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
