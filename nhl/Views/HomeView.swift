import SwiftUI

struct HomeView: View {
    
    @State var menuOpen: Bool = false
    
    var body: some View {
        
        NavigationView {
            
            ZStack {
                Text("").navigationBarTitle(Text("Home"), displayMode: .inline)
                        .navigationBarItems(leading: Button(action: {
                            self.openMenu()
                        }, label: { Image("menu") }))
                    
                    SideMenu(width: 322,
                             isOpen: self.menuOpen,
                        menuClose: self.openMenu)
                    }
        }.navigationViewStyle(StackNavigationViewStyle())
    }
    
    func openMenu() {
        self.menuOpen.toggle()
    }
}


struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            
            HomeView().previewLayout(.fixed(width: 375, height: 1000)).environment(\.colorScheme, .dark)
        }
    }
}
