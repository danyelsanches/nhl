import Foundation
import SwiftUI

struct SortPopoverView: View {
    
    var sortPlayers: (String) -> ()
    
    var body: some View {
        
        VStack (alignment: .center, spacing: 20) {
                
            Text("Sort Options")
                .font(.callout)
                .foregroundColor(.gray)
            
            Divider()

            Text("Sort by name")
                .foregroundColor(.blue)
                .onTapGesture {
                    self.sortPlayers("name")
                }

            Divider()

            Text("Sort by #")
                .foregroundColor(.blue)
                .onTapGesture {
                    self.sortPlayers("jerseyNumber")
                }
                
        }
    }
        
}

