import SwiftUI

struct TeamRow: View {
    var team: Team

    var body: some View {
        HStack {
                team.image
                    .resizable()
                    .frame(width: 50, height: 50)
                    .padding([.horizontal])
            
                VStack(alignment: .leading){
                    Text(team.name)
                    Text(team.abbreviation)
                        .font(.subheadline)
                        .foregroundColor(.gray)
                        
                }
        }
    }
}

struct TeamRow_Previews: PreviewProvider {
    static var previews: some View {
        
        TeamRow(team: Team(dict: ["id": 1, "name": "New York", "abbreviation": "NJD"]))
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
