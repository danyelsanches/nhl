import Foundation
import SwiftUI

struct FilterPopoverView: View {
    
    @State var positionList: [Position] = []
    var filterBy: (String) -> ()
    var resetFilter: () -> ()
    
    var body: some View {
         
        VStack (alignment: .center, spacing: 20) {
            
            Text("Filter by position")
                .font(.callout)
                .foregroundColor(.gray)
            
            Divider()
            
            Text("All (Reset Filter)")
                .foregroundColor(.blue)
                .onTapGesture {
                    self.resetFilter()
                }
            
            ForEach(self.positionList, id: \.code) { position in

                    Text(position.name)
                    .foregroundColor(.blue)
                    .onTapGesture {
                        self.filterBy(position.code)
                    }
                
            }
        }
        
    }
}
