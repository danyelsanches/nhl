import SwiftUI

public struct TeamPlayerView: View {
    
    @EnvironmentObject var playerStore: PlayerStore
    @EnvironmentObject var personStore: PersonStore
    @State var team: Team
    
    @State var showPersonDetail = false
    @State var showIndicator: Bool = false
    @State var showPersonDetailIndicator: Bool = false
    
    public var body: some View {
        
        LoadingView(isShowing: self.$showIndicator) {
            
            List(self.playerStore.playerMap()) { p in

                HStack {
                    PlayerRow(player: p)
                }
                .onTapGesture {
                    self.personStore.person = nil // Clear the last detail
                    self.showPersonDetailPopover(player: p as Player)
                }
            
            }
        
        }
        .navigationBarTitle(Text("Players"), displayMode: .automatic)
        .navigationBarItems(trailing: HStack {
            
            Button(action: {
               
                self.playerStore.showPopoverOrder = true
                
            }, label: { Image("sort").resizable().frame(width: 20, height: 20)})
            .padding(.horizontal, 10)
            
            Button(action: {
                
                self.playerStore.showPopoverFilter = true

            }, label: { Image("filter").resizable().frame(width: 20, height: 20)})
            
        })
        .popover(isPresented: .constant(self.playerStore.showPopoverFilter || self.playerStore.showPopoverOrder), attachmentAnchor: .point(.trailing), arrowEdge: .bottom, content: {

                if ( self.playerStore.showPopoverFilter ) {
                
                FilterPopoverView(positionList: self.playerStore.positionList, filterBy: { code in
                    self.playerStore.filterBy(code: code)
                }, resetFilter: {
                    self.playerStore.resetFilter()
                })
                
            } else {
                
                SortPopoverView(sortPlayers: {sortCriteria in
                    self.playerStore.sortCriteria = sortCriteria
                    self.playerStore.sortPlayers()
                })
                
            }

        })
        .sheet(isPresented: self.$showPersonDetail, onDismiss: {}, content: {

            PlayerDetailView(onDismiss: {
                self.showPersonDetail.toggle()
            }).environmentObject(self.personStore)

        })
        .onAppear(perform: fetchPlayers)
        
    }
    
    // MARK: - Fetch
    
    func showPersonDetailPopover(player: Player){
        self.showPersonDetailIndicator = true
        self.personStore.fetchPerson(personId: player.person.id){
            self.showPersonDetailIndicator = false
            self.showPersonDetail = true
        }
    }
    
    func fetchPlayers() {
        self.showIndicator = true
        self.playerStore.fetchPlayers(teamId: self.team.id){
            self.showIndicator = false
        }
    }
}
