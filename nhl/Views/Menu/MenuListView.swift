import SwiftUI
import Foundation
import Alamofire

struct MenuListView: View {
    
    @EnvironmentObject var teamStore: TeamStore
    @State private var showIndicator: Bool = false
    @State private var showError: Bool = false
    
    var body: some View {
        
        ZStack {
            
            LoadingView(isShowing: self.$showIndicator) {
                List(self.teamStore.teams, id: \.id) { team in
                    NavigationLink(destination: TeamPlayerView(team: team)) {
                        TeamRow(team: team)
                    }
                }
            }
        
        }
        .alert(isPresented: self.$showError, content: {
            Alert(title: Text("Error..."), message: Text("Cannot connect to server!"), dismissButton: .default(Text("Ok")))
        })
        .onAppear(perform: fetch)
        
    }
    
    private func fetch(){
    
        self.showIndicator = true
        self.teamStore.fetchTeams {resultStatus in
            self.showIndicator = false
            self.showError = !resultStatus
        }
        
    }
    
}
