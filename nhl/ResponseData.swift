//
//  ResponseData.swift
//  nhl
//
//  Created by Daniel Eduardo Verissimo De Castro S DAbadia on 26/09/19.
//  Copyright © 2019 Daniel Eduardo Verissimo De Castro S DAbadia. All rights reserved.
//

import Foundation

struct ResponseData {
    let error: Error
    let data: Any?
    
    init(data: Any?, error: Error) {
        self.error = error
        self.data = data
    }
}
