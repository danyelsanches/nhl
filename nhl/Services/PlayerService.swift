import Alamofire

class PlayerService {
    
    /// Get an array of all players.
    ///
    /// - Parameters:
    ///     - result: Closute to delivery an array of teams `Player` object.
    public static func getPlayers(teamId: Int, result:@escaping (Result<Any, Error>)->Void) {
        
        let route = APIClient.baseUrl + "/teams/\(teamId)/roster"
        
        APIClient.performRequest(route: route){ response in
            
            switch response {
                case .success(let data):
                    
                    guard let value = data as? [String: Any],
                        let items = value["roster"] as? [[String: Any]] else {
                        return
                    }
                    
                    let player = items.compactMap { dict in return Player(dict: dict) }
                    result(.success(player))
                    
                    break
                case .failure(let error):
                    result(.failure(error))
                    break
            }
        }
    }
}
