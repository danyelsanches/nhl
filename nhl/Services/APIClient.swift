import Alamofire

class APIClient {
    
    public static var baseUrl: String = "https://statsapi.web.nhl.com/api/v1"
    
    /// Perform a request returning a JSON object.
    ///
    /// - Parameters:
    ///     - route: Route string to a resource.
    ///     - complete: Closute to call a success or failures response.
    public static func performRequest(route: String, complete:@escaping (Result<Any, Error>)->Void) -> Void {
        AF.request(route)
            .responseJSON { response in
                
                if ( response.response?.statusCode == 200) {
                    complete(.success((response.value!)))
                } else {
                    complete(.failure(response.error!))
                }
            }
    }
}
