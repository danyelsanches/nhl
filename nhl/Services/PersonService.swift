import Alamofire

class PersonService {
    
    /// Get person data  from a server api.
    ///
    /// - Parameters:
    ///     - result: Closute to delivery an `Person` object.
    public static func getPerson(personId: Int, result:@escaping (Result<Any, Error>)->Void) {
        
        let route = APIClient.baseUrl + "/people/\(personId)"
        
        APIClient.performRequest(route: route){ response in
            
            switch response {
                case .success(let data):
                    
                    guard let value = data as? [String: Any],
                        let items = value["people"] as? [[String: Any]] else {
                        return
                    }
                    
                    result(.success(Person(dict: items[0])))
                    break
                
                case .failure(let error):
                    result(.failure(error))
                    break
            }
        }
    }
}
