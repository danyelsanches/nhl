import Alamofire

class TeamService {
    
    /// Get an array of all teams.
    ///
    /// - Parameters:
    ///     - result: Closute to delivery an array of teams `Team` object.
    public static func getTeams(result:@escaping (Result<Any, Error>)->Void) {
        
        let route = APIClient.baseUrl + "/teams"
        
        APIClient.performRequest(route: route){ response in
            
            switch response {
                case .success(let data):
                    
                    guard let value = data as? [String: Any],
                        let items = value["teams"] as? [[String: Any]] else {
                        return
                    }
                    let teams = items.compactMap { dict in return Team(dict: dict) }
                    result(.success(teams))
                    break
                case .failure(let error):
                    result(.failure(error))
                    break
            }
        }
    }
    
}
